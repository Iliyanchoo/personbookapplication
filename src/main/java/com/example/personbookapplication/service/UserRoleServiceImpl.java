package com.example.personbookapplication.service;

import com.example.personbookapplication.models.entities.UserRole;
import com.example.personbookapplication.models.enums.UserRoleEnums;
import com.example.personbookapplication.repositories.UserRoleRepository;
import com.example.personbookapplication.service.contracts.UserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepository userRoleRepository;

    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public void seedRoles() {
        if (userRoleRepository.count() == 0){
            UserRole adminRole = new UserRole().setRole(UserRoleEnums.ADMIN);
            UserRole userRole = new UserRole().setRole(UserRoleEnums.USER);
            UserRole creatorRole = new UserRole().setRole(UserRoleEnums.CREATOR);

            userRoleRepository.saveAll(List.of(adminRole, userRole, creatorRole));
        }
    }

    @Override
    public Optional<UserRole> getByUserRole(UserRoleEnums role) {
        return Optional.of(userRoleRepository.findByRole(role).orElseThrow());
    }
}
