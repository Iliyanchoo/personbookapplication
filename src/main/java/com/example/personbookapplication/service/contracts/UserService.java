package com.example.personbookapplication.service.contracts;

import com.example.personbookapplication.models.view_models.UserViewModel;

import java.util.List;

public interface UserService {

    List<UserViewModel> getAll();

    UserViewModel getByUsername(String username);

    void seedUsers();

}
