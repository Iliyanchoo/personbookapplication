package com.example.personbookapplication.service.contracts;

import com.example.personbookapplication.models.entities.UserRole;
import com.example.personbookapplication.models.enums.UserRoleEnums;

import java.util.Optional;

public interface UserRoleService {
    void seedRoles();

    Optional<UserRole> getByUserRole(UserRoleEnums role);
}
