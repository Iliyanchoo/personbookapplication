package com.example.personbookapplication.service;

import com.example.personbookapplication.exceptions.EntityNotFoundException;
import com.example.personbookapplication.models.entities.User;
import com.example.personbookapplication.models.entities.UserRole;
import com.example.personbookapplication.models.enums.UserRoleEnums;
import com.example.personbookapplication.models.view_models.UserViewModel;
import com.example.personbookapplication.repositories.UserRepository;
import com.example.personbookapplication.service.contracts.UserRoleService;
import com.example.personbookapplication.service.contracts.UserService;
import com.google.gson.Gson;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    private final UserRoleService userRoleService;
    private final ModelMapper modelMapper;

    private final Gson gson;

    private final Resource usersFile;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, UserRoleService userRoleService, ModelMapper modelMapper,
                           Gson gson, @Value("classpath:init/users.json") Resource usersFile) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRoleService = userRoleService;
        this.modelMapper = modelMapper;
        this.gson = gson;
        this.usersFile = usersFile;
    }

    @Override
    public List<UserViewModel> getAll() {
        List<User> users = userRepository.findAllUsers();
        List<UserViewModel> userViewModels = new ArrayList<>();

        users.forEach(user -> userViewModels.add(modelMapper.map(user, UserViewModel.class)));

        return userViewModels;
    }

    @Override
    public UserViewModel getByUsername(String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() ->
                new EntityNotFoundException("User with name ", username, " was not found!"));

        return modelMapper.map(user, UserViewModel.class);
    }

    @Override
    public void seedUsers() {
        if (userRepository.count() == 0) {
            try {
                UserRole adminRole = userRoleService.getByUserRole(UserRoleEnums.CREATOR).orElseThrow();
                User[] usersEntities =
                        gson.fromJson(Files.readString(Path.of(usersFile.getURI())), User[].class);

                Arrays.stream(usersEntities)
                        .forEach(u -> u.setPassword(passwordEncoder.encode(u.getPassword())));

                Arrays.stream(usersEntities)
                        .forEach(u -> u.setRoles(List.of(adminRole)));

                userRepository.saveAll(Arrays.asList(usersEntities));

            } catch (Exception e){
                throw new IllegalStateException("Cannot seed users");
            }
        }
    }
}
