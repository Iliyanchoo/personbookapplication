package com.example.personbookapplication.models.entities;

import com.example.personbookapplication.models.enums.UserRoleEnums;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "roles")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type = "uuid-char")
    @Column(name = "role_id")
    private UUID id;

    @Column(name = "role_name")
    @Enumerated(EnumType.STRING)
    private UserRoleEnums role;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users = new HashSet<>();

    public UserRole setRole(UserRoleEnums role){
        this.role = role;
        return this;
    }

    @Override
    public String toString() {
        return role.name();
    }
}
