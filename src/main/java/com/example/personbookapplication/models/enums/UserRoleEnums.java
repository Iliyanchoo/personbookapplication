package com.example.personbookapplication.models.enums;

public enum UserRoleEnums {
    ADMIN, USER, CREATOR
}
