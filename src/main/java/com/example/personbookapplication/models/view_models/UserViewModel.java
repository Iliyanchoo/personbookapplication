package com.example.personbookapplication.models.view_models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserViewModel {

    private String username;
    private String password;
    private String firstName;
    private String middleName;
    private String lastName;
}
