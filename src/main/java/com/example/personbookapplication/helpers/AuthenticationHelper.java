package com.example.personbookapplication.helpers;

import com.example.personbookapplication.exceptions.AuthorizationException;
import com.example.personbookapplication.exceptions.EntityNotFoundException;
import com.example.personbookapplication.models.entities.User;
import com.example.personbookapplication.models.view_models.UserViewModel;
import com.example.personbookapplication.service.contracts.UserService;
import org.hibernate.SessionFactory;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationHelper {

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String INVALID_AUTHENTICATION_ERROR = "Invalid authentication.";
    private final UserService userService;
    private final SessionFactory sessionFactory;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;

    public AuthenticationHelper(UserService userService, SessionFactory sessionFactory, PasswordEncoder passwordEncoder, ModelMapper modelMapper) {
        this.userService = userService;
        this.sessionFactory = sessionFactory;
        this.passwordEncoder = passwordEncoder;
        this.modelMapper = modelMapper;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
        }

        try {
            String userInfo = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            String username = getUsername(userInfo);
            String password = getPassword(userInfo);
            UserViewModel user = userService.getByUsername(username);

            if (!passwordEncoder.matches(password, user.getPassword())) {
                throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
            }

            return modelMapper.map(user, User.class);
        } catch (EntityNotFoundException e) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
        }
    }

    private String getUsername(String userInfo) {
        int firstSpace = userInfo.indexOf(" ");
        if (firstSpace == -1) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
        }

        return userInfo.substring(0, firstSpace);
    }

    private String getPassword(String userInfo) {
        int firstSpace = userInfo.indexOf(" ");
        if (firstSpace == -1) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
        }
        return userInfo.substring(firstSpace + 1);
    }
}
