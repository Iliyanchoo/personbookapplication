package com.example.personbookapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
public class PersonBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonBookApplication.class, args);
    }

}
