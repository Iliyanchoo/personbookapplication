package com.example.personbookapplication.repositories;


import com.example.personbookapplication.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    @Query("SELECT u from User u")
    List<User> findAllUsers();

    Optional<User> findByUsername(String username);
}
