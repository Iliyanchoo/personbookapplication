package com.example.personbookapplication.repositories;

import com.example.personbookapplication.models.entities.UserRole;
import com.example.personbookapplication.models.enums.UserRoleEnums;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRoleRepository extends JpaRepository<UserRole, UUID> {
    Optional<UserRole> findByRole(UserRoleEnums user);

}
