package com.example.personbookapplication.controllers.rest;

import com.example.personbookapplication.exceptions.AuthorizationException;
import com.example.personbookapplication.helpers.AuthenticationHelper;
import com.example.personbookapplication.models.view_models.UserViewModel;
import com.example.personbookapplication.service.contracts.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    public UserRestController(UserService userService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/get/all")
    public List<UserViewModel> getAllUsers(@RequestHeader("Authorization") HttpHeaders headers){

        try{
            authenticationHelper.tryGetUser(headers);
            return userService.getAll();
        } catch (AuthorizationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
