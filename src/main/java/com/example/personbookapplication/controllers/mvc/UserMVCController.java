package com.example.personbookapplication.controllers.mvc;

import com.example.personbookapplication.service.contracts.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/users")
public class UserMVCController {

    private final UserService userService;

    private final ModelMapper modelMapper;

    public UserMVCController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }


}
